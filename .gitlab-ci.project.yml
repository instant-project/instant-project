# ======================
# CI Stages
# ======================

stages:
  - test
  - build
  - deploy

# ======================
# Snippets
# ======================

.composer-install-dev: &composer-install-dev |
  composer install \
    --working-dir src \
    --prefer-dist \
    --no-progress \
    --no-suggest \
    --no-interaction \
    --ignore-platform-reqs \
    --no-scripts

.composer-install-no-dev: &composer-install-no-dev |
  composer install \
    --working-dir src \
    --no-dev \
    --prefer-dist \
    --no-progress \
    --no-suggest \
    --no-interaction \
    --ignore-platform-reqs \
    --no-scripts

.cache: &cache
  cache:
    key: test
    paths:
      - src/vendor/


# ======================
# Testing
# ======================

test:
  stage: test
  image: existenz/builder:7.3
  services:
    - docker:dind
  <<: *cache
  before_script:
    - *composer-install-dev
  script:
    - make do-run-tests


# ======================
# Build images
# ======================

app:
  stage: build
  image: existenz/builder:7.3
  <<: *cache
  only:
    refs:
      - master
  services:
    - docker:dind
  before_script:
    - *composer-install-no-dev
    - export DOCKER_TAG="$CI_PIPELINE_ID";
    - docker login -u "$CI_REGISTRY_USER" -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - docker build
      --file ops/docker/Dockerfile
      -t $CI_REGISTRY_IMAGE/app:$DOCKER_TAG
      -t $CI_REGISTRY_IMAGE/app:master
      --target prod
      .
    - docker push $CI_REGISTRY_IMAGE/app:$DOCKER_TAG
    - docker push $CI_REGISTRY_IMAGE/app:master


# ======================
# Deploy images
# ======================

deploy:
  image: enrise/kube-toolbox:digital-ocean
  stage: deploy
  environment:
    name: PROD
    url: https://$INSTANT_PROJECT_DOMAIN
  only:
    refs:
      - master
  before_script:
    - mkdir ~/.kube
    - cp $PROD_KUBE_CONFIG_FILE ~/.kube/config
  script:
    - export DOCKER_TAG="$CI_COMMIT_REF_SLUG"
    - envsubst < ops/kube/config.yml > config.yml
    - kubectl apply -f config.yml
    - kubectl rollout status deployment -n $INSTANT_PROJECT_SLUG $INSTANT_PROJECT_SLUG-deployment


# ======================
# Plumbing details
# ======================

variables:
  DOCKER_HOST: "tcp://docker:2375"
