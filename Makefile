
# First command in the file is the default. so please leave this here :-)
info: intro help

# ===========================
# Main commands
# ===========================

help: intro ## This help text
	@awk 'BEGIN {FS = ":.*?[#][#][ ]?"} \
	 /^[a-zA-Z_-]*:?.*?[#][#][ ]?/ {printf "\033[33m%-15s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST) |\
	 sed --regexp-extended 's/[#]{2,}[ ]*//g'

##
## Project commands
init: intro src/.env do-update-project ## Make the project ready for development.
update: intro do-switch-branch do-update-project ## Update the project, run as 'update BRANCH=<branch>' to checkout a branch first.
clean: intro do-clean ## Cleanup caches etc.

##
## Control dev environment
start: intro do-docker-start ## Start the Docker containers.
stop: intro do-docker-stop ## Stop the Docker containers.
restart: stop start ## Restart the Docker containers.

##
## Tests
test: intro do-run-tests ## Run the test commands (default none).

##
## Dev commands
logs: intro do-logs ## Tail the logs of all containers.
shell: intro do-shell ## Open a shell in the app container.
builder-shell: intro do-builder-shell ## Open a shell in the builder container, primarily so you can run composer.


# ===========================
# Snippets
# ===========================

set-ids = USERID=$$(id -u) GROUPID=$$(id -g)

# ===========================
# Recipes
# ===========================

do-update-project: do-docker-build do-install-dependencies do-docker-start
do-install-dependencies: src/.env do-install-composer-dependencies

# Project commands

src/.env: do-check-software-dependencies .env.dist
	@echo "\n=== Please select this environment's variables ===\n"
	@docker run --rm \
		--user $$(id -u):$$(id -g) \
		--volume $$PWD:/app \
		--interactive --tty \
		evaneos/companienv \
		--file=src/.env

do-check-software-dependencies:
	@if [ ! `command -v git` ]; then \
	    echo "\nPlease install git\n"; \
	    return 1; \
	fi
	@if [ ! `command -v docker` ]; then \
	    echo "\nPlease install docker\n"; \
	    return 1; \
	fi
	@if [ ! `command -v docker-compose` ]; then \
	    echo "\nPlease install docker-compose\n"; \
	    return 1; \
	fi

do-switch-branch:
	@if [ -z $$BRANCH ]; then \
	    echo "\n=== Running updates for the current branch ===\n"; \
	    echo "No branch is set, run: 'make update BRANCH=<branch>' to checkout and update a branch"; \
	else \
	    echo "\n=== Switching to and updating $$BRANCH ===\n"; \
	    git checkout $$BRANCH; \
	    git pull origin $$BRANCH; \
	fi

do-clean: do-docker-stop
	@echo "\n=== Cleaning up caches etc. ===\n"
	@find .Makefile -type f -not -name '.gitignore' -delete
	@${set-ids} docker-compose --file dev/docker-compose.yml rm -f
	@rm -rf src/vendor/*

# Run development environment

do-docker-build: src/.env
	@echo "\n=== (Re)building services ===\n"
	@${set-ids} docker-compose --file dev/docker-compose.yml build

do-docker-start: src/.env do-install-composer-dependencies
	@echo "\n=== Starting containers ===\n"
	@${set-ids} docker-compose --file dev/docker-compose.yml up -d --remove-orphans

do-docker-stop: src/.env
	@echo "\n=== Stopping containers ===\n"
	@${set-ids} docker-compose --file dev/docker-compose.yml stop

do-install-composer-dependencies: src/.env composer-cache-dir
	@echo "\n=== Installing dependencies ===\n"
	@docker run --rm --interactive --tty \
		--user $$(id -u):$$(id -g) \
		--volume $$PWD/src:/app \
		--volume $${COMPOSER_HOME:-$$HOME/.composer}:/.composer \
		existenz/builder:7.3 \
		composer --working-dir=/app install --prefer-dist --ignore-platform-reqs --no-suggest

composer-cache-dir:
	@mkdir --parents $${COMPOSER_HOME:-$$HOME/.composer}

# Tests

do-run-tests:
	@echo "\n=== Run tests ===\n"
	@echo "Placeholder message\n"

# Dev tools
do-logs:
	@echo "\n=== Watch logs ===\n"
	@${set-ids} docker-compose --file dev/docker-compose.yml logs -f --tail 5

do-shell:
	@echo "\n=== Start shell in app container ===\n"
	@${set-ids} docker-compose --file dev/docker-compose.yml exec app sh

do-builder-shell: src/.env composer-cache-dir
	@echo "\n=== Start shell in builder container ===\n"
	@docker run --rm --interactive --tty \
		--user $$(id -u):$$(id -g) \
		--volume $$PWD/src:/app \
		--volume $${COMPOSER_HOME:-$$HOME/.composer}:/.composer \
		existenz/builder:7.3 \
		sh

# ===========================
# Commands needed for the Makefile itself
# ===========================

.env.dist:
	$(error Error: .env.dist not found. Please re-create it or remove all references to it from your Makefile)

.Makefile/headerfile:
	$(error Error: .Makefile/headerfile not found. Please re-create it (by running \
		.Makefile/create_header.sh) or remove all references to it from your Makefile)

intro: .Makefile/headerfile do-check-software-dependencies
	@echo "\033[33m"
	@cat .Makefile/headerfile
	@echo "Dev url:       \033[0m http://dev.$INSTANT_PROJECT_DOMAIN/"
	@echo "\n"
