

# PROJECT ON HOLD
NOTICE: I have shut down the kubernetes cluster associated with this project, so while the code will still work, example projects etc. will not.
As I have continued this project in its spirtual successor (paid for by my job), it is unlikely to return.

---

# instant-project
    Quickly bootstrap a deployable, containerized (PHP) project.  

Ever had a fun idea for a small web-app, chatbot or other project but never started it because setting up a proper
project structure take so much time?

Or have you ever worked on a project that started as a quick one-off and found yourself annoyed that it lacked niceties
like handy make commands, a CI/CD pipeline, containerization, automatic deployment or the ability to scale it up?

Than this is the project for you!

Like the name says, **instant-project** allows you to go from idea to good, future-proof, ready-to-go project in minutes!


## What do you get
The initial state will be clean, but ready for development. In short:
- a git repo with no history of instant-project
- a Make file with:
  - the most common commands
  - a clean structure
  - a 'test' command that is also used in the CI/CD pipeline
  - a pretty ASCII art header :-P
- local development reachable via a `dev.your-prod-url.com` hostname
- `src/` directory with composer initialized
- docker-compose based dev environment, with a good default container setup and `src/` mounted in the container for live editing
- a Gitlab pipeline with test, build and deploy steps
- automated deployment to Kubernetes
- tools chosen for portability


## What don't you get
* **updates:** after you run `init.sh` all traces of instant-project will be gone, so any improvements to 
instant-project will have to be copied manually.
* **tutorials:** although this project allows you to use some stuff without fully understanding how it works, this is 
not a training/example project.
* **choices:** you cannot choose to use this _"...but with a mysql container"_, or _"...but deploying to Heroku instead 
of Kubernetes"_. You can, however, edit everything yourself :-).
* **support:** Good luck!


## Requirements
These tools are required for both the project you are going to initialize and the initializing itself.  
- bash
- GNU Make
- git
- Docker
- Docker-compose
- Free port 80 (nginx)
- have [hostsupdater](https://github.com/nstapelbroek/hostupdater) running connected to a docker network called `hostsupdater`
- a Gitlab account
- a Kubernetes cluster to deploy to
  - note: details for recommended config and easy/automated setup of cluster are on my TODO list :-P
- kubectl with access to that cluster (at the moment you run init.sh) 
- a (sub-) domain name


## Usage
- clone this project
- choose a name for your intended project, ie: foo-bar
- run `mv instant-project foo-bar && cd foo-bar && ./init.sh`
- create the project in Gitlab (ideally with the same name) and add it as a git remote
- add a kube config file to your Gitlab project CI/CD variables like so:
  - type: File
  - key: PROD_KUBE_CONFIG_FILE
  - value: the config file that gives you access to your kubernetes cluster, usually stored in ~/.kube/config
  - state/masked: I only tested with both off
  - scope: "All environments"
  - you can skip this in the future by creating all projects under a Gitlab group and setting the CI/CD var on that group
- point the domain name you chose (when asked by init.sh) to your cluster
  - you can skip this in the future by pointing a wildcard sub domain record at your cluster
- run `git push`
