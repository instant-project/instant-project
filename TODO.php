<?php


# TODO[Johan] focus houden op einddoel!

# WKFbF7-LtfxSzHBYVosJ

/** ==== MVP  ===== **/




/** ==== FAIL FAST, eerst feedback verzamelen en dat oppaken samen met onderstaande  ===== **/

// TODO[Johan] composer json/lock niet in sync?

// TODO[Johan] kijken of alle INSTANT_PROJECT_ vars nog wel in .env.dist gezet moeten worden
//   of iig in versie die gecommit wordt
//   en of .env.dist wel gekopieerd moet worden met companienv?

// TODO[Johan] geheel op Enrise Gitlab zetten

# TODO[Johan] bestand .Makefile/tools.yml aanmaken dat stiekem docker-compose.yml is
#   * alle "services" daarin zijn tools die door het project gebruikt worden, zoals test commando's, code stylers, etc
#   * in Makefile worden alle commando's (die normaal ter plekke gedefineerd worden) daarheen doorgesluist
#   * zoals eerder gebruikt .gitlab-ci dan weer Makefile commando's
#   * mogelijk losse script files maken die ook dit file gebruiken, zodat alles consistent en in docker draait
#   * zie https://youtu.be/EfpVgKtqZVw?t=3173

# TODO[Johan] hoe cluster opzetten ergens in repo vastleggen:
#   * DO cluster init
#   * kubeconfig file in gitlab zetten
#   * image pull secret aanmaken (zie create-secret.sh in instant-project repo, op basis van deploy token op Gitlab group nivo)
#   * moet imagepullsecret instellen mbv dit: https://github.com/titansoft-pte-ltd/imagepullsecret-patcher
#   * kubectl create clusterrolebinding kube-system-default-account-as-cluster-admin \
#       --clusterrole=cluster-admin \
#       --serviceaccount=kube-system:default
#   * helm install traefik-ingress --values traefik-config.yml stable/traefik --namespace kube-system
#       * traefik-config.yml staat in instant-project repo, moet in "zet het cluster op" repo
#       * Traefik pak (met die config) in principe alle ingresses op in alle namespaces
#   * misschien default backend maken? https://docs.traefik.io/v1.7/configuration/backends/kubernetes/#global-default-backend-ingresses
#   * met automatisch HTTPS
#       initieel kan DO dat automatisch doen
#       https://akomljen.com/get-automatic-https-with-lets-encrypt-and-kubernetes-ingress/

# TODO[Johan] .gitignore dingen pimpen
# TODO[Johan] make status commando maken
# TODO[Johan] cache-tool gebruiken https://github.com/eXistenZNL/Docker-Builder
#    .cache-tool-before-script: &cache-tool-before-script |
#    cache-tool extract composer:~/.composer
#    .cache-tool-after-script: &cache-tool-after-script |
#    cache-tool collect composer:~/.composer
# TODO[Johan] composer install commando's op Gitlab identieke manier draaien als lokaal
#   (met dezelfde PHP setup als app container)
# TODO[Johan] mergeback bot opnemen
# TODO[Johan] combinatie minikube, ingress-dns (is misschien niet gestart?) en devspace uitproberen
# TODO[Johan] als goed genoeg werkt, docker-compose vervangen door minikube et al.
# TODO[Johan] kustomize gaan gebruiken om kubernetes manifests gelijk te trekken
# TODO[Johan] software eisen updaten (minikube, hostupdater, devspace)



/** ==== Done  ===== **/

// TODO[Johan]  huidige project opschonen

// TODO[Johan] TODO ergens veiligstellen

// TODO[Johan] repo verplaatsen naar Gitlab instant-project group

// TODO[Johan] je moet ips kopieren :-/
//   misschien live kopieren tijdens init?
//  https://stackoverflow.com/a/52326812
//  auto copy ding moet geinstalled en dan dit teruggedraait

// TODO[Johan] do-check-software-dependencies moet falen als er niet is

// TODO[Johan] cronjob weghalen; YAGNI

// TODO[Johan] laatste test-init doen met ander project

# TODO[Johan] .env.dist.template weghalen in init.sh (en algemeen: alle gerelateerde dingen uit Makefile halen)

// TODO[Johan] .gitlab-ci.project.yml hernoemen om pipelines te voorkomen

// TODO[Johan] uitkomst van cluster inrichten opschonen
//  README.md checken of leesbaarheid
//  checken of nog commits in bar zijn die overgenomen moeten worden
//  kijken of nog iets moet met files in kubernetes-sandbox-setup-WIP/ dir


# TODO[Johan] README.md updaten
#   composer.json hoeft niet meer
#   moet project in gitlab aanmaken
#   moet domeinnaam laten verwijzen naar loadbalancer ip (als geen wildcard hebt ingesteld)


# TODO[Johan] - daarna verder uitzoeken wat er nodig is om de "bar" deployments ook opgepikt te krijgen door traefik
# TODO[Johan] - zorgen dat ik duidelijk houd hoe ik het cluster in de staat gekregen heb waarin ik dingen kan deployen (zoals het geven van rechten aan de default serviceAccount in kube-system)


# TODO[Johan] draaien op digital ocean
#   algemene ingress router maken, etc.

# TODO[Johan] als eerste kijken waarom "docker build" e.d. niet werken in gitlab CI
#   uitzetten van services leek niks te doen, misschien start ik de service niet goed op?
#       gitlab-ci syntax bekijken

# TODO[Johan] heel basic kubernetes manifest maken
#   spm ACC versie ombouwen
#   nalopen op dit: https://kubernetes.io/blog/2019/07/18/api-deprecations-in-1-16/


# image pull secret: KNkNHxE_mgLi-LTYfoXT
# foo.instant-project

# foo-instant-project-deploy-token
# GoYfE2ysxizy1yPmDzMS

