#!/bin/sh

run_companienv_to_create_env_dist () {
  docker run \
    --rm --interactive --tty \
    --user "$(id -u)":"$(id -g)" \
    --volume "$(pwd)":/app \
    evaneos/companienv \
    --dist-file=.env.dist.template \
    --file=.env.dist
}

substitute_instant_variables () {
  docker run --rm -it \
    --user "$(id -u)":"$(id -g)" \
    -v "$(pwd)":/tmp \
    --env-file .env.dist \
    enrise/kube-toolbox:digital-ocean \
      sh -c "envsubst '\$INSTANT_PROJECT_NAME \$INSTANT_PROJECT_SLUG \$INSTANT_PROJECT_DOMAIN' < /tmp/$1 > /tmp/$1.tmp && mv /tmp/$1.tmp /tmp/$1";
}

run_companienv_to_create_env_dist && \
  substitute_instant_variables dev/docker-compose.yml && \
  substitute_instant_variables src/composer.json && \
  substitute_instant_variables ops/kube/config.yml && \
  substitute_instant_variables Makefile && \
  mv .gitlab-ci.project.yml .gitlab-ci.yml && \
  substitute_instant_variables .gitlab-ci.yml && \
  rm README.md && mv README.project.md README.md && \
  substitute_instant_variables README.md && \
  substitute_instant_variables .Makefile/create_header.sh && \
  chmod +x .Makefile/create_header.sh && \
  ./.Makefile/create_header.sh && \
  rm -rf .git && \
  rm TODO.php && \
  rm .env.dist.template && \
  sed -i '/^\.env\.dist/d' .gitignore && \
  rm init.sh && \
  git init && \
  git add --all && \
  git commit -m ":tada: Initial commit"
